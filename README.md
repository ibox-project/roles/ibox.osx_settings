osx_settings
=========

Clone git repositories.

Requirements
------------

Repositories are public accessible or authentication is provided.
Git executable must be installed.

Role Variables
--------------

| Variable       | Required | Description | Default |
|----------------|----------|-------------|---------|
| osx_settings__ | true     |             | ''      |
| osx_settings__ | true     |             | []      |

Dependencies
------------

```yaml
```

Example Playbook
----------------
Include the role in `requirements.yml` with something like
```yaml
  - src: https://gitlab.com/ibox-project/roles/ibox.osx_settings.git
    version: stable
    scm: git
    name: ibox.osx_settings
```

and write in the playbook something like

```yaml
  tasks:
    - name: "Include ibox.osx_settings"
      ansible.builtin.include_role:
        name: "ibox.osx_settings"
 
```

License
-------

Licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
